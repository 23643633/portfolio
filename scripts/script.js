
const hamburger = document.querySelector(".hamburger");         //looks for the element that contains hamburger and takes it as a variable
let hamburgerOpen = false;                                      //sets the original state of the hamburger to closed
hamburger.addEventListener("click", function() {                //checks to see if the hamburger element has been clicked
  if(!hamburgerOpen) {                                          //if the hamburger is not open and has been clicked
  hamburger.classList.add("open");                              //then add the open class to change styles
  hamburger.classList.add("is-active");                         //
  document.getElementById("mobileNav").style.width = "100%";    // Move the nav menu across the screen
  document.getElementById("mobileNav").style.position = "fixed" // fix its position
  hamburgerOpen = true;                                         // sets the state to open
} else {
  hamburger.classList.remove("open");                           //if already open then remove the open status
  hamburger.classList.remove("is-active");
  document.getElementById("mobileNav").style.width = "0%";      // remove the menu from the screen
  hamburgerOpen = false;                                        // set the hamburger to closed
}
});

$(function() {                      //Function for toggling subnav in mobile mode
  if($(window).width()<=768){       //Checks to see if width is lower than desired
    $('.nested').click(function(){ //checks to see if the element with the nested class is clicked
      $('.subnav').slideToggle(); //toggles the nav if this is the case
    });
  };
});
